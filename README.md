SALAXY STARTER SITE (salaxy-web-blank)
======================================

This project contains a minimal set of code for running Salaxy web application.

Getting started
---------------

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-web-blank`
2. Enter project directory `cd salaxy-web-blank`
3. Run `npm install` for installing required grunt-modules
3. Start the project web site in dev web server: `grunt server`
  - Server starts in the port 9001
4. Have fun in exploring and developing with the code.