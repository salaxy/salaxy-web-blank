/** 
 * This is a minimal grunt config just to start web server
 * Add your own stuff as you wish.
 */
module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({
        gruntServerPort: 9001,
        less: {
            www: {
                options: {
                    sourceMap: true,
                    sourceMapFileInline: false,
                    sourceMapRootpath: "/",
                },
                files: {
                    "www/css/bootstrap.css": "src/less/_bootstrap.less",
                    "www/css/salaxy-lib-ng1-bootstrap.css": "src/less/_salaxy-component-bootstrap.less",   
                    "www/css/salaxy-rpt.css": "src/less/_salaxy-rpt.less",
                    "www/css/extra.css": "src/less/extra/extra.less"     
                }
            }
        },
        postcss: {
            www: {
                options: {
                    map: {
                        inline: false,
                    },
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')(),
                    ]
                },
                files: {
                    "www/css/bootstrap.css": "www/css/bootstrap.css",
                    "www/css/salaxy-lib-ng1-bootstrap.css": "www/css/salaxy-lib-ng1-bootstrap.css",
                    "www/css/extra.css": "www/css/extra.css"   
                }
            },

        },

        ts: {
            www: {
                src: [
                    'src/ts/**/*.ts',
                ],
                outDir: '.build',
                options: {
                    allowSyntheticDefaultImports: true,
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: true,
                    declaration: true,
                    removeComments: false,
                    moduleResolution: 'node',
                    lib: ["es2015", "dom"],
                    rootDir: "./src/ts",
                    skipLibCheck: true
                },
            }, 
            config: {
                src: [
                    'src/env/global/salaxy.ts',
                ],
                outDir: '.config-build',
                options: {
                    allowSyntheticDefaultImports: true,
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: true,
                    declaration: true,
                    removeComments: false,
                    moduleResolution: 'node',
                    lib: ["es2015", "dom"],
                    rootDir: "./src/env",
                    skipLibCheck: true
                },
            }   
        },
        browserify: {
            www: {
                src: [".build/**/*.js"],
                dest: 'www/js/salaxy-app.js',
                options: {
                    external: ["@salaxy/core","@salaxy/ng1"],                   
                    browserifyOptions: {
                        bundleExternal: false,
                    },
                    transform: ['browserify-shim']          
                }
            },
            config: {
                src: [".config-build/**/*.js"],
                dest: 'www/js/salaxy-config.js',
                options: {
                    external: ["@salaxy/core"],                   
                    browserifyOptions: {
                        bundleExternal: false,
                    },
                    transform: ['browserify-shim']          
                }
            },
        },

        clean: {
            build: {
                src: ['.build','.config-build']
            },
        },
        csvjson: {
            options: {
            },
            default: {
                src: 'src/ts/i18n/salaxy-translation.csv',
                dest: 'src/ts/i18n'
            }
        },
        json: {
            www: {
                options: {
                    namespace: '_dictionary',
                    includePath: false
                },
                src: ['src/ts/i18n/*.json'],
                dest: 'src/ts/i18n/dictionary.ts'
            }
        },
        concat: {
            dictionary: {
                options: {
                    banner: "/* tslint:disable */\n",
                    footer: "\nexport const dictionary = _dictionary\n"
                },
                src: ["src/ts/i18n/dictionary.ts"],
                dest: "src/ts/i18n/dictionary.ts",
                nonull: true
            }
        },
        copy: {
            lib: {
                files: [
                    { expand: true, cwd: 'node_modules/@salaxy/ng1/js', src: 'salaxy-lib-ng1-all.js', dest: 'www/js/' },
                ],
            },
            content: {
                files: [
                    { expand: true, cwd: 'src/assets', src: ['**'], dest: 'www/' },
                    { expand: true, cwd: 'src/views', src: ['**'], dest: 'www/views/' },
                    { expand: true, flatten: true, src: ['src/index.html'], dest: 'www/' }
                ],
            },
        },
        connect: {
            all: {
                options: {
                    port: '<%= gruntServerPort%>',
                    base: 'www',
                    hostname: "localhost",
                    livereload: true
                }
            },
        },
        watch: {
            all: {
                files: ['www/**/*.*' ],
                options: {
                    livereload: true
                },
            },
            json: {
                files: ['src/ts/i18n/**/*.json'],
                tasks: ['json','concat'],
            },
            ts: {
                files: ['src/ts/**/*.ts'],
                tasks: ['clean','ts','browserify', 'clean'],
            },
            less: {
                files: ['src/less/**/*.less'],
                tasks: ['less','postcss'],
            },
            content: {
                files: ['src/**/*.html','src/assets/**/*.*'],
                tasks: ['copy:content'],
            },
        },
        open: {
            all: {
                path: 'http:/<%= connect.all.options.hostname%>:<%= connect.all.options.port%>/index.html'
            }
        }

    });
  
    grunt.loadNpmTasks('grunt-csv-json');
    grunt.registerTask('i18n', ['csvjson', 'json', 'concat']);

    var tasks = ['less','postcss','clean','i18n','ts','browserify','clean','copy'];
   
    grunt.registerTask('default', tasks);
    grunt.registerTask('build', tasks);

    grunt.registerTask('server', ['connect', 'open', 'watch']);
    
};