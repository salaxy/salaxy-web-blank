import * as angular from "angular";

import {
    AccountService,
    CalculationsService,
    CmsService,
    NaviService,
    OnboardingService,
    PayrollService,
    ReportsService,
    TaxCardService,
    WorkersService,
} from "@salaxy/ng1";

/** Handles the route change event by extracting the route parameter and sets the current object in repositories. */
export class OnRouteChange {

    protected static $inject = [
        "$rootScope",
        "$route",
        "$routeParams",
        "NaviService",
        "CalculationsService",
        "WorkersService",
        "AccountService",
        "CmsService",
        "ReportsService",
        "OnboardingService",
        "TaxCardService",
        "PayrollService",
    ];

    constructor(
        $rootScope: angular.IRootScopeService,
        $route: angular.route.IRouteService,
        $routeParams: angular.route.IRouteParamsService,
        naviService: NaviService,
        calculationsService: CalculationsService,
        workersService: WorkersService,
        accountService: AccountService,
        cmsService: CmsService,
        reportsService: ReportsService,
        onboardingService: OnboardingService,
        taxCardService: TaxCardService,
        payrollService: PayrollService,
    ) {

        /* YOUR ROUTING LOGIC HERE - THIS IS JUST AN EXAMPLE */

        // Handle the parameters coming in route parameters (typically selected business object id)
        const checkRoute = () => {

            if ($routeParams.newCalcForWorkerId) {
                calculationsService.createNew($routeParams.newCalcForWorkerId);
            }
            if ($routeParams.calculationId) {
                if (calculationsService.getCalculations().length === 0) {
                    calculationsService.reloadList().then( () => {
                        calculationsService.setCurrent($routeParams.calculationId, true);
                    });
                } else {
                    calculationsService.setCurrent($routeParams.calculationId, true);
                }
            }
            if ($routeParams.workerId) {
                workersService.setCurrent($routeParams.workerId, false);
            }
            if ($routeParams.productId) {
                accountService.setCurrent($routeParams.productId);
            }
            if ($routeParams.articleId) {
                cmsService.setCurrent($routeParams.articleId);
            }
            if ($routeParams.reportType) {
                reportsService.currentReportType = $routeParams.reportType;
            }
            if ($routeParams.payrollId) {
                if (payrollService.list.length === 0) {
                    payrollService.reloadList().then( () => {
                        payrollService.setCurrentId($routeParams.payrollId);
                    });
                } else {
                    payrollService.setCurrentId($routeParams.payrollId);
                }
            }
            if ($routeParams.onboardingId) {
                if ( $routeParams.onboardingId === "new") {
                    onboardingService.id = null;
                    onboardingService.getOnboardingData();
                } else {
                    onboardingService.id = $routeParams.onboardingId;
                    onboardingService.getOnboardingData();
                }
            }
            if ($routeParams.companyOnboardingId) {
                if ( $routeParams.companyOnboardingId === "new") {
                    onboardingService.id = null;
                    onboardingService.createNewCompanyOnboarding();
                } else {
                    onboardingService.id = $routeParams.companyOnboardingId;
                    onboardingService.getOnboardingData();
                }
            }

        };
        $rootScope.$on("$routeChangeSuccess", checkRoute);
    }
}
