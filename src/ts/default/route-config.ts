
import * as angular from "angular";

/** Route configuration */
export class RouteConfig {
    protected static $inject = ["$routeProvider"];

    constructor($routeProvider: angular.route.IRouteProvider) {

        const getTemplate = (section, template?) => {
            return "./views/default/" + section + "/" + (template || "index") + ".html";
        };

        $routeProvider
            .when("/", { templateUrl: getTemplate("home") })
            .when("/index", { templateUrl: getTemplate("home") })
            .when("/error", { templateUrl: getTemplate("home", "error") })

            .when("/calculations/details/:calculationId", { templateUrl: getTemplate("calculations", "details") })
            .when("/calculations/newFor/:newCalcForWorkerId", { templateUrl: getTemplate("calculations", "details") })
            .when("/calculations/:viewName", { templateUrl: (path) => getTemplate("calculations", path.viewName) })
            .when("/payroll", { templateUrl: (path) => getTemplate("calculations", "payroll") })
            .when("/payroll/details/:payrollId", { templateUrl: (path) => getTemplate("calculations", "payroll-edit") })
            .when("/payroll/new", { templateUrl: (path) => getTemplate("calculations", "payroll-edit") })

            .when("/workers/details/:workerId", { templateUrl: (path) => getTemplate("workers", "detail") })
            .when("/workers/taxcards",  { templateUrl: getTemplate("workers", "taxcardslist") })
            .when("/workers/:viewName", { templateUrl: (path) => getTemplate("workers", path.viewName) })
            .when("/workers/:viewName/:workerId", { templateUrl: (path) => getTemplate("workers", path.viewName) })

            .when("/partners/index", { templateUrl: getTemplate("partners") })
            .when("/partners/authorizingAccounts", {templateUrl: getTemplate("partners", "authorizing-accounts")})
            .when("/partners/authorizedAccounts", {templateUrl: getTemplate("partners", "authorized-accounts")})

            .when("/settings/info/:productId", { templateUrl: getTemplate("settings", "info") })
            .when("/settings/language", { templateUrl: getTemplate("settings", "language") })
            .when("/settings/:viewName", { templateUrl: (path) => getTemplate("settings", path.viewName) })
            .when("/products/:viewName", { templateUrl: (path) => getTemplate("settings", path.viewName) })
            .when("/account/:viewName", { templateUrl: (path) => getTemplate("settings", path.viewName) })

            .when("/reports/:viewName", { templateUrl: (path) =>  getTemplate("reports", path.viewName)})

            .when("/company-onboarding/:companyOnboardingId", { templateUrl: getTemplate("onboarding", "company-onboarding") })
            .when("/company-onboarding/:companyOnboardingId/:step", { templateUrl: getTemplate("onboarding", "company-onboarding") })

            .when("/cms", { templateUrl: getTemplate("cms") })
            .when("/cms/index", { templateUrl: getTemplate("cms") })
            .when("/cms/:articleId", { templateUrl: getTemplate("cms", "article") })

            .otherwise({ templateUrl: getTemplate("home", "error404") });
    }
}
