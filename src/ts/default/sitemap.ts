const _sitemap = [
    {
        title: "APP.Navi.home",
        url: "#/",
        id: "home",
    },
    {
        title: "APP.Navi.calculations.index",
        url: "#/calculations/details",
        id: "calculations",
        children: [
            {
                title: "APP.Navi.calculations.new",
                url: "#/calculations/details/new",
            },
            {
                title: "APP.Navi.calculations.classic",
                url: "#/calculations/details/*",
            },
            {
                title: "APP.Navi.calculations.pro",
                url: "#/calculations/details/#pro",
            },
            {
                title: "APP.Navi.calculations.paid",
                url: "#/calculations/paid",
            },
            {
                title: "APP.Navi.calculations.drafts",
                url: "#/calculations/draft",
            },
            {
                title: "APP.Navi.calculations.received",
                url: "#/calculations/received",
                hidden: true,
            },
            {
                title: "APP.Navi.calculations.payroll",
                url: "#/payroll",
                children: [
                    {
                        title: "Palkkalistan muokkaus / maksu",
                        url: "#/payroll/details/*",
                        hidden: true,
                    },
                ],
            },
            {
                title: "APP.Navi.calculations.newFor",
                url: "#/calculations/newFor",
                hidden: true,
            },
        ],
    },
    {
        title: "APP.Navi.workers.index",
        url: "#/workers/index",
        id: "workers",
        children: [
            {
                title: "APP.Navi.workers.allWorkers",
                url: "#/workers/index",
            },
            {
                title: "APP.Navi.workers.add",
                url: "#/workers/details/new",
            },
            {
                title: "APP.Navi.workers.detail",
                url: "#/workers/details/*",
                hidden: true,
            },
            {
                title: "APP.Navi.workers.contracts",
                url: "#/workers/contract",
                hidden: true,
            },
            {
                title: "APP.Navi.workers.taxcards",
                url: "#/workers/taxcards/",
            },
            {
                title: "APP.Navi.workers.employment",
                url: "#/workers/employment/*",
                hidden: true,
            },
        ],
    },
    {
        title: "APP.Navi.reports.index",
        url: "#/reports/kirjanpito",
        id: "reports",
        children: [
            {
                title: "APP.Navi.reports.accounting",
                url: "#/reports/kirjanpito",
            },
            {
                title: "APP.Navi.reports.pension",
                descr: "Eläkevakuutusilmoitukset",
                url: "#/reports/tyel-ilmoitukset",
            },
            {
                title: "APP.Navi.reports.unemployment",
                descr: "TVR-ilmoitukset",
                url: "#/reports/tvr-ilmoitukset",
            },
            {
                title: "APP.Navi.reports.monthly",
                url: "#/reports/vk-ilmoitukset",
            },
            {
                title: "APP.Navi.reports.yearly",
                url: "#/reports/vv-ilmoitukset",
            },
            {
                title: "APP.Navi.reports.yearlyWorker",
                url: "#/reports/vuosiyhteenvedot",
            },
        ],
    },
    {
        title: "APP.Navi.settings.index",
        url: "#/settings/index",
        id: "settings",
        children: [
            {
                title: "APP.Navi.settings.baseService",
                url: "#/settings/info/baseService",
                hidden: true,
            },
            {
                title: "APP.Navi.settings.accountInfo",
                url: "#/settings/accountInfo",
            },
            {
                title: "APP.Navi.settings.userInfo",
                url: "#/settings/userInfo",
            },
            {
                title: "APP.Navi.settings.language",
                url: "#/settings/language",
                hidden: true,
            },
            {
                title: "APP.Navi.settings.tax",
                url: "#/settings/info/tax",
            },
            {
                title: "APP.Navi.settings.unemployment",
                url: "#/settings/info/unemployment",
            },
            {
                title: "APP.Navi.settings.pension",
                url: "#/settings/info/pension",
            },
            {
                title: "APP.Navi.settings.insurance",
                url: "#/settings/info/insurance",
            },
            {
                title: "APP.Navi.settings.accounting",
                url: "#/settings/info/accounting",
            },
            {
                title: "APP.Navi.settings.healthCare",
                url: "#/settings/info/healthCareHeltti",
            },
            {
                title: "APP.Navi.settings.pensionEntrepreneur",
                url: "#/settings/info/pensionEntrepreneur",
            },
            {
                title: "Valtuutukset ja varmenteet",
                url: "#/account/authorizationsAndCertificates",
                hidden: true,
            },
        ],
    },
];

export const sitemap = _sitemap;
