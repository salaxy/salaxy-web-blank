import {AjaxNg1, SessionService} from "@salaxy/ng1";
import * as angular from "angular";

/** Handles the session authentication event. */
export class OnAuthenticatedSession {

    protected static $inject = [
        "$rootScope",
        "$window",
        "SessionService",
        "AjaxNg1"];

    constructor(
        $rootScope: angular.IRootScopeService,
        $window: angular.IWindowService,
        sessionService: SessionService,
        ajax: AjaxNg1 ) {

        // show the Onborading if signature has not been done.
        sessionService.onAuthenticatedSession($rootScope, () => {

            if (!sessionService.getSession().currentAccount) {
                return;
            }
            if (
                sessionService.getSession().currentAccount.identity.contract &&
                sessionService.getSession().currentAccount.identity.contract.isSigned
            ) {
                return;
            }
            /*
            // uncommented: for bookkeeping companies
            const url = ajax.getServerAddress() + "/Onboarding/new?access_token=" + ajax.getCurrentToken();
            $window.location.href = url;
            */

        });
    }
}
