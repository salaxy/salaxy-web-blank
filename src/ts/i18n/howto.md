Grunt file contains the following tasks for generating dictionaries:

- csvjson: Converts the csv-file to json-files. One json file will be created for each language (for each column after the name column).
- json: Convertes the json-files to one typescript file: dictionary.ts.
- concat: Finalizes the dictionary.ts file for exporting the dictionary.
- i18n: A composite task which will execute all the tasks above.

The command `grunt build` will generate the dictionaries.
You can also separately run the composite task i18n using the command `grunt i18n`.