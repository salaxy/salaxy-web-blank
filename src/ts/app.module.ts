import * as angular from "angular";

import { Ng1Translations} from "@salaxy/ng1";

import { OnAuthenticatedSession } from "./default/on-authenticated-session";
import { OnRouteChange } from "./default/on-route-change";
import { RouteConfig } from "./default/route-config";
import { sitemap } from "./default/sitemap";
import { dictionary } from "./i18n/dictionary";

/** The main angular module configuration. */
export const module = angular.module("salaxyApp", ["ngRoute", "salaxy.ng1.components.all"])

    // Set sitemap for navigation
    .constant("SITEMAP", sitemap)
    // Set routes to pages
    .config(RouteConfig)
    // Set authenticated event management
    .run(OnAuthenticatedSession as any)
    // Set parameter extraction from paths when route changes
    .run(OnRouteChange as any)
    // Set application specific translations
    .run(["Ng1Translations", (translate: Ng1Translations) => {
        translate.addDictionaries(dictionary);
        translate.setLanguage("fi");
    }]);
    // Other application specific services, controllers and components
    // .controller("SampleController", SampleController);
