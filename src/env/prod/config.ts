import { Config } from "@salaxy/core";

/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export const config: Config = {
    wwwServer: "https://www.palkkaus.fi",
    apiServer: "https://secure.salaxy.com",
    isTestData: false,
    useCookie: false,
};
